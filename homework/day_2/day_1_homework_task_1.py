def counting_letters(text):
    """
    This function converts string into a dictionary with symbols as keys and its chances
    to appear in a string as a value
    :param text: str
    :return: dict
    """
    converted_dict = {}
    for key in set(text):
        converted_dict[key] = round((text.count(key) / len(text)) * 100, 2)
    return converted_dict


if __name__ == "__main__":
    tests = (
        ("One", {"O": 33.33, "n": 33.33, "e": 33.33}),
        ("Three", {"T": 20, "h": 20, "r": 20, "e": 40}),
        ("Eleven", {"E": 16.67, "l": 16.67, "e": 33.33, "v": 16.67, "n": 16.67}),
    )
    for word, result in tests:
        func_res = counting_letters(word)
        assert (
            func_res == result
        ), f"ERROR: counting_letters({word}) returned {func_res}, but expected: {result}"

    try:
        counting_letters(33)
    except TypeError as te:
        print(f"Incorrect input type: {te}. Input must be only string.")
